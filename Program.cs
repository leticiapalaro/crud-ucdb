using CPP.Models;
using Microsoft.EntityFrameworkCore;

var builder = WebApplication.CreateBuilder(args);

// Todo servi�o que for utilizado precisa ser registrado na classe 'program'
// Bloco de c�digo que repassa a aplica��o o uso do contexto baseado no EF
// <DataContext> = classe de manipula��o da aplica��o

builder.Services.AddDbContext<DataContext>(options => // definindo options
{
    // definindo que a estrat�gia de inicializa��o do contexto ir� usar o SqlServer
    /* GetConnectionString("DefaultConnection")
     * mostrando onde est� o banco de dados - string de conex�o
     * a mesma definida em CRUDUCDB\appsettings.json */

    options.UseSqlServer(builder.Configuration.GetConnectionString("DefaultConnection"));
});


// Add services to the container.
builder.Services.AddControllersWithViews();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (!app.Environment.IsDevelopment())
{
    app.UseExceptionHandler("/Home/Error");
    // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
    app.UseHsts();
}

app.UseHttpsRedirection();
app.UseStaticFiles();

app.UseRouting();

app.UseAuthorization();

app.MapControllerRoute(
    name: "default",
    pattern: "{controller=Home}/{action=Index}/{id?}");

app.Run();
