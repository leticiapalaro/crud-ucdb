﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace CPP.Migrations
{
    public partial class Segunda : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Pedidos",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    NomeProduto = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    Valor = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    Desconto = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    MostraDesconto = table.Column<decimal>(type: "decimal(18,2)", nullable: true),
                    ValorFinal = table.Column<decimal>(type: "decimal(18,2)", nullable: true),
                    DataVencimento = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    StatusPedido = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Pedidos", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Pedidos");
        }
    }
}
