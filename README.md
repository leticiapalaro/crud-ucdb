Ferramentas Utilizadas:

Visual Studio Community 2022
https://visualstudio.microsoft.com/pt-br/vs/community/

SQL Server 2019 Developer
https://www.microsoft.com/pt-br/sql-server/sql-server-downloads

SQL Server Management Studio (SSMS) 18.12.1
https://docs.microsoft.com/pt-br/sql/ssms/download-sql-server-management-studio-ssms?view=sql-server-ver16

SDK .NET 6.0.400
https://dotnet.microsoft.com/en-us/download/dotnet/6.0

---

Pacotes Utilizados:

Microsoft.EntityFrameworkCore

Microsoft.EntityFrameworkCore.SqlServer

Microsoft.EntityFrameworkCore.Design

Microsoft.EntityFrameworkCore.Tools

---

1 - Instalar as ferramentas utilizadas

2 - Abrir o projeto através do arquivo **CPP.sln**

2 - No VS abrir o Console do Gerenciador de Pacotes (Ferramentas > Gerenciador de Pacotes do NuGet > Console do Gerenciador de Pacotes), no console fazer a primeira migração (Add-Migration *nome da migração*) e o primeiro update (Update-Database).

3 - Rodar o sistema através do VS.
