﻿using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System.Xml.Linq;

namespace CPP.Models
{
    public class Pedido
    {
        [Key]
        public int Id { get; set; }

        // Nome do Produto
        [Required(ErrorMessage = "Campo obrigatório, preencha o nome do produto")]
        [Display(Name = "Produto")]
        public string NomeProduto { get; set; }

        // Valor do Produto
        private decimal _valor; // Utilizado para calcular o Valor Final
        [Required(ErrorMessage = "Campo obrigatório, preencha o valor do produto")]
        [Display(Name = "Valor")]
        [RegularExpression(@"^(\d{1,3}(\.\d{3})*|\d+)(\,\d{2})?$")]
        public string Valor { get ; set ; }

        // Desconto
        [Display(Name = "Desconto")]
        [RegularExpression(@"^(\d{1,3}(\.\d{3})*|\d+)(\,\d{2})?$")]
        public string? Desconto { get; set; }

        private decimal? _desconto;
        [Display(Name = "Desconto")]
        [Column(TypeName = "decimal(18,2)")]
        public decimal? MostraDesconto { get => _desconto; set => SetMostraDesconto(); }

        private decimal? SetMostraDesconto()
        {
            DateTime data_vencimento_toDateTime = DateTime.Parse(_data_vencimento);
            DateTime data_atual = DateTime.Now;
            TimeSpan diferenca_datas = data_vencimento_toDateTime.Subtract(data_atual);

            if (diferenca_datas.Days >= 0 && diferenca_datas.Days <= 3)
            {
                _desconto = Convert.ToDecimal(Desconto);
            }
            else if (diferenca_datas.Days < 0)
            {
                _desconto = 0;
            }
            else
            {
                _desconto = Convert.ToDecimal(Desconto);
            }

            return _desconto;
        }


        // Valor Final
        private decimal? _valor_final;
        [Display(Name = "Valor Final")]
        [Column(TypeName = "decimal(18,2)")]
        public decimal? ValorFinal { get => _valor_final; private set => SetValorFinal(); }
        public decimal? SetValorFinal()
        {
            _valor = Convert.ToDecimal(Valor);
            if (_desconto > _valor | _status_pedido == "Pedido Vencido" | _desconto == null)
            {
                _valor_final = _valor;
            }
            else
            {
                _valor_final = _valor - _desconto;
            }
            return _valor_final;
        }

        // Data de Vencimento do Pedido
        private string _data_vencimento;
        [Required(ErrorMessage = "Campo obrigatório, preencha a data de vencimento do pedido")]
        [Display(Name = "Vencimento")]
        [DataType(DataType.Date)]
        public string DataVencimento { get => _data_vencimento; set => _data_vencimento = value; }

        // Pedidos Válidos, Vencidos e Próximos do Vencimento
        private string _status_pedido;
        [Display(Name = "Status")]
        public string? StatusPedido { get => _status_pedido; private set => SetStatusPedido(); }

        public string SetStatusPedido()
        {
            DateTime data_vencimento_toDateTime = DateTime.Parse(_data_vencimento);
            DateTime data_atual = DateTime.Now;
            TimeSpan diferenca_datas = data_vencimento_toDateTime.Subtract(data_atual);

            if (diferenca_datas.Days >= 0 && diferenca_datas.Days <= 3)
            {
                _status_pedido = "Próximo do Vencimento";
            }
            else if (diferenca_datas.Days < 0)
            {
                _status_pedido = "Pedido Vencido";
            }
            else
            {
                _status_pedido = "Pedido Válido";
            }

            return _status_pedido;
        }
    }
}
