﻿using Microsoft.EntityFrameworkCore;

namespace CPP.Models
{
    public class DataContext : DbContext
    // Classe que permite a manipulação de dados através da ORM EF, herdando as configurações da classe DbContext
    {
        // Snippet para criar construtor 'ctor'
        // Definindo a estratégia de inicialização do Bd no construtor
#pragma warning disable CS8618 // O campo não anulável precisa conter um valor não nulo ao sair do construtor. Considere declará-lo como anulável.
        public DataContext(DbContextOptions<DataContext> options) : base(options)
#pragma warning restore CS8618 // O campo não anulável precisa conter um valor não nulo ao sair do construtor. Considere declará-lo como anulável.
        {

        }

        // Fazendo mapeamento da classe com o Banco de Dados
        // Para compilar a aplicação Ctrl+Shift+B
        public DbSet<Pedido> Pedidos { get; set; }
    }
}
